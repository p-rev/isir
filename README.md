# ISIR 

Moteur de lancer de rayon écrit en C++ avec Model de Phong, BRDF de Cook-Torrance, transparance et reflection.

![Alt text](https://bitbucket.org/p-rev/isir/raw/master/results/pierrick_reveillere_IMAGE_PROJET_ISIR.png)

### Dépendances

Pour compiler le projet sur linux il faut :

* CMake
* GCC
* GNU Make

### Utilisation

Paramètres :

* nom de l'image
* hauteur
* largeur
* iterations max
* suréchantillonnage

exemple :

```
$ ./isir result.png 800 600 5 2
```

## Auteur

* **Pierrick Réveillère** - [p-rev](https://github.com/p-rev)