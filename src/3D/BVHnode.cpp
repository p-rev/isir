#include "BVHnode.hpp"

#include <cfloat>

#include <algorithm>
#include <iostream>
#include <string>

namespace ISICG_ISIR {

    BVHnode::BVHnode(std::vector<IndexTriangle*>* triangles,
            const std::vector<Vec3f>& vertices,
            BoundingBox* box, bool sorted)
    {
        create(triangles, vertices, box, sorted);
    }

    BVHnode::~BVHnode()  {
        if (_child0) delete _child0;
        if (_child1) delete _child1;
        if (_box) delete _box;
        if (_triangles) delete _triangles;
    }

    void BVHnode::create(std::vector<IndexTriangle*>* triangles,
            const std::vector<Vec3f>& vertices,
            BoundingBox* box, bool sorted)
    {
        const uint minNodeSize = 8;

        // on peut potentiellement réutiliser les données calculées en dessous pour le SAH pour faire la boite du noeud
        if (box == nullptr) {
            box = new BoundingBox;
            for (auto i = triangles->begin(); i != triangles->end(); ++i) {
                box->enlarge(vertices[(*i)->_v0]);
                box->enlarge(vertices[(*i)->_v1]);
                box->enlarge(vertices[(*i)->_v2]);
            }
        }
        _box = box;

        if (triangles->size() <= minNodeSize) {
            _child0 = nullptr;
            _child1 = nullptr;
            _triangles = triangles;
            return;
        }

        if (!sorted) {
            std::sort(triangles->begin(), triangles->end(),
                [vertices](IndexTriangle* a, IndexTriangle* b) {
                    const float aminx = glm::min(
                        glm::min(vertices[a->_v0].x, vertices[a->_v1].x), vertices[a->_v2].x);
                    const float bminx = glm::min(
                        glm::min(vertices[b->_v0].x, vertices[b->_v1].x), vertices[b->_v2].x);
                    return aminx < bminx;
                });
        }

        const uint nbTri = triangles->size();
        const uint nbBox = nbTri - 1;
        std::vector<BoundingBox> boxes0(nbBox);
        std::vector<BoundingBox> boxes1(nbBox);

        // init Boxes 0
        boxes0[0].enlarge(vertices[(*triangles)[0]->_v0]);
        boxes0[0].enlarge(vertices[(*triangles)[0]->_v1]);
        boxes0[0].enlarge(vertices[(*triangles)[0]->_v2]);

        for (uint i = 1; i < nbBox; ++i) {
            boxes0[i] = boxes0[i - 1];
            boxes0[i].enlarge(vertices[(*triangles)[i]->_v0]);
            boxes0[i].enlarge(vertices[(*triangles)[i]->_v1]);
            boxes0[i].enlarge(vertices[(*triangles)[i]->_v2]);
        }

        // init Boxes 1
        boxes1[0].enlarge(vertices[(*triangles)[nbTri - 1]->_v0]);
        boxes1[0].enlarge(vertices[(*triangles)[nbTri - 1]->_v1]);
        boxes1[0].enlarge(vertices[(*triangles)[nbTri - 1]->_v2]);

        for (uint i = 1; i < nbBox; ++i) {
            uint j = nbTri - i - 1;
            boxes1[i] = boxes1[i - 1];
            boxes1[i].enlarge(vertices[(*triangles)[j]->_v0]);
            boxes1[i].enlarge(vertices[(*triangles)[j]->_v1]);
            boxes1[i].enlarge(vertices[(*triangles)[j]->_v2]);
        }


        float area = box->area();
        uint minI = 0;
        float minSAH = FLT_MAX;
        for (uint i = 0; i < nbBox; ++i) {
            uint j = nbBox - 1 - i;
            uint nb0 = i;
            uint nb1 = nbTri - nb0;
            float area0 = boxes0[i].area();
            float area1 = boxes1[j].area();

            float SAH = (area0 * nb0 + area1 * nb1) / area;

            if (SAH < minSAH) {
                minSAH = SAH;
                minI = i;
            }
        }
        std::vector<IndexTriangle*> *triangles0 = new std::vector<IndexTriangle*>(minI + 1);
        std::vector<IndexTriangle*> *triangles1 = new std::vector<IndexTriangle*>(nbTri - minI - 1);

        for(uint i = 0; i < minI + 1; ++i) {
            (*triangles0)[i] = (*triangles)[i];
        }

        for(uint i = minI + 1; i < nbTri; ++i) {
            (*triangles1)[i - minI - 1] = (*triangles)[i];
        }

        delete triangles;

        _child0 = new BVHnode(triangles0, vertices, nullptr, true);
        _child1 = new BVHnode(triangles1, vertices, nullptr, true);
    }

    void BVHnode::intersect(const Ray &r, std::vector<IndexTriangle*> &triangles) const {
        if (_box == nullptr) {
            std::cout << "Pas de box\n";
        }
        if (_box->intersect(r)) {
            if (_child0) {
                _child0->intersect(r, triangles);
                _child1->intersect(r, triangles);
            } else {
                for (auto i = _triangles->begin(); i != _triangles->end(); ++i) {
                    triangles.push_back(*i);
            }
        }
        }
    }

}
