#ifndef __ISICG_ISIR_BVH_NODE__
#define __ISICG_ISIR_BVH_NODE__

#include <vector>

#include "defines.hpp"
#include "boundingBox.hpp"
#include "indexTriangle.hpp"
#include "ray.hpp"

namespace ISICG_ISIR {
    class BVHnode {
    private:
        BVHnode* _child0 = nullptr;
        BVHnode* _child1 = nullptr;
        BoundingBox* _box = nullptr;
        std::vector<IndexTriangle*>* _triangles = nullptr;

    public:
        BVHnode() = default;
        BVHnode(std::vector<IndexTriangle*>* triangles,
                const std::vector<Vec3f>& vertices,
                BoundingBox* box = nullptr, bool sorted = false);

        void create(std::vector<IndexTriangle*>* triangles,
                const std::vector<Vec3f>& vertices,
                BoundingBox* box = nullptr, bool sorted = false);

        void intersect(const Ray &r, std::vector<IndexTriangle*> &triangles) const;

        ~BVHnode();


    };
}

#endif //__ISICG_ISIR_BOUNDING_BOX__
