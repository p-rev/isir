#ifndef __ISICG_ISIR_A_OBJECT_3D__
#define __ISICG_ISIR_A_OBJECT_3D__

#include "intersection.hpp"
#include "ray.hpp"
#include "../defines.hpp"

namespace ISICG_ISIR
{
    class AMaterial;

	class AObject3D
	{
	public:

		void setMaterial(AMaterial *const material)
		{
			_material = material;
		}

		// Compute ray/object intersection
		// return parameter t and set normal if intersection exists
		virtual std::vector<Intersection> intersect(const Ray &ray)
			= 0;

		AMaterial*  getMaterial() {
			return _material;
		}

	protected:
		AMaterial *_material = nullptr; // pointer for polymorphism
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_A_OBJECT_3D__
