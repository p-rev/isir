#include "boundingBox.hpp"

#include <sstream>
#include <algorithm>

namespace ISICG_ISIR {
    void BoundingBox::enlarge(const Vec3f& p) {
        _minx = glm::min(_minx, p.x);
        _maxx = glm::max(_maxx, p.x);
        _miny = glm::min(_miny, p.y);
        _maxy = glm::max(_maxy, p.y);
        _minz = glm::min(_minz, p.z);
        _maxz = glm::max(_maxz, p.z);
    }

    void BoundingBox::toString(std::string& s) const {
        std::ostringstream ss;
        ss << "minx " << _minx << ", maxx " << _maxx << "\n" <<
            "miny " << _miny << ", maxy " << _maxy << "\n" <<
            "minz " << _minz << ", maxz " << _maxz << "\n";
        s = std::string(ss.str());
    }

    bool BoundingBox::isEmpty() const {
        return _minx > _maxx;
    }

    /*TODO à optimiser https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection*/

    bool BoundingBox::intersect(const Ray& r) const {
        float tmin = (_minx - r._position.x) / r._direction.x;
        float tmax = (_maxx - r._position.x) / r._direction.x;

        if (tmin > tmax) std::swap(tmin, tmax);

        float tymin = (_miny - r._position.y) / r._direction.y;
        float tymax = (_maxy - r._position.y) / r._direction.y;

        if (tymin > tymax) std::swap(tymin, tymax);

        if ((tmin > tymax) || (tymin > tmax))
            return false;

        if (tymin > tmin)
            tmin = tymin;

        if (tymax < tmax)
            tmax = tymax;

        float tzmin = (_minz - r._position.z) / r._direction.z;
        float tzmax = (_maxz - r._position.z) / r._direction.z;

        if (tzmin > tzmax) std::swap(tzmin, tzmax);

        if ((tmin > tzmax) || (tzmin > tmax))
            return false;

        /* pas besoin ?
        if (tzmin > tmin)
        tmin = tzmin;

        if (tzmax < tmax)
        tmax = tzmax;
        */

        return true;
    }

    float BoundingBox::area() const {
        float x = glm::abs(_maxx - _minx);
        float y = glm::abs(_maxy - _miny);
        float z = glm::abs(_maxz - _minz);

        return 2.0f * (x*z + z*y + y*x);
    }
}
