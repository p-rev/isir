#ifndef __ISICG_ISIR_BOUNDING_BOX__
#define __ISICG_ISIR_BOUNDING_BOX__

#include <cfloat>

#include <string>

#include "defines.hpp"
#include "ray.hpp"

namespace ISICG_ISIR {
    class BoundingBox {
    private:
        float _minx =  FLT_MAX;
        float _maxx = -FLT_MAX;
        float _miny =  FLT_MAX;
        float _maxy = -FLT_MAX;
        float _minz =  FLT_MAX;
        float _maxz = -FLT_MAX;


    public:
        BoundingBox () {}
        virtual ~BoundingBox () {}

        void enlarge(const Vec3f& p);
        void toString(std::string& s) const;
        bool isEmpty() const;
        bool intersect(const Ray& r) const;
        float area() const;
    };
} /* ISICG_ISIR */

#endif // __ISICG_ISIR_BOUNDING_BOX__
