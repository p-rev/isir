#include "implicit.hpp"

namespace ISICG_ISIR
{
    std::vector<Intersection> Implicit::intersect(const Ray &ray)
    {
        std::vector<Intersection> inter;

        if (_box.intersect(ray)) {
            float depth = 0.0f;

            for (uint i = 0; i < MAX_ITERATION; ++i) {
                Vec3f p = ray._position + depth * ray._direction;
                float dist = this->sdf(p);

                if (dist < EPSILON) {
                    inter.push_back(Intersection(estimateNormal(p), glm::length(p - ray._position), this));
                    break;
                }

                depth += dist;
            }
        }

        return inter;
    }

    float Implicit::sdf(const Vec3f &p) const {
        const float PHI = glm::sqrt(5.f) * .5f + .5f;
        Vec3f q = p - _origin;

        Vec3f pp = glm::abs(q);
        if (pp.x < glm::max(pp.y, pp.z)) pp = Vec3f(pp.y, pp.z, pp.x);
        if (pp.x < glm::max(pp.y, pp.z)) pp = Vec3f(pp.y, pp.z, pp.x);

        float b = glm::max(glm::max(glm::max(
            glm::dot(pp, glm::normalize(Vec3f(1.f, 1.f, 1.f))),
            glm::dot(Vec2f(pp.x, pp.z), glm::normalize(Vec2f(PHI + 1.f, 1.f)))),
            glm::dot(Vec2f(pp.y, pp.x), glm::normalize(Vec2f(1.f, PHI)))),
            glm::dot(Vec2f(pp.x, pp.z), glm::normalize(Vec2f(1.f, PHI))));

        float l = glm::length(pp);

        return (l - 1.5f - 0.2f * (1.5f / 2.0f) * glm::cos(glm::min(glm::sqrt(1.01f - b / l) * (PI / 0.25f), PI)));
//        return (glm::length(p - _origin) - 1.0f);
    }

    Vec3f Implicit::estimateNormal(const Vec3f &p) const {
        return glm::normalize(Vec3f(
            sdf(Vec3f(p.x + EPSILON, p.y, p.z)) - sdf(Vec3f(p.x - EPSILON, p.y, p.z)),
            sdf(Vec3f(p.x, p.y + EPSILON, p.z)) - sdf(Vec3f(p.x, p.y - EPSILON, p.z)),
            sdf(Vec3f(p.x, p.y, p.z + EPSILON)) - sdf(Vec3f(p.x, p.y, p.z - EPSILON))
        ));
    }

} // namespace ISICG_ISIR
