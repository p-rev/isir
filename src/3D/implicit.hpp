#ifndef __ISICG_ISIR_IMPLICIT__
#define __ISICG_ISIR_IMPLICIT__

#include <vector>

#include "aObject3D.hpp"
#include "boundingBox.hpp"

namespace ISICG_ISIR
{
    class Implicit : public AObject3D
    {
    public:
        Implicit(const Vec3f origin, float size) {
            _origin = origin;

            _box.enlarge(origin + Vec3f(1.0f) * (size / 2));
            _box.enlarge(origin - Vec3f(1.0f) * (size / 2));
        }

        std::vector<Intersection> intersect(const Ray &ray) override;

    private:
        const uint MAX_ITERATION = 256;
        Vec3f _origin;
        glm::mat4 _transform;
        BoundingBox _box;

        float sdf(const Vec3f &p) const;
        Vec3f estimateNormal(const Vec3f &p) const;
    };
} // namespace ISICG_ISIR

#endif //__ISICG_ISIR_IMPLICIT__
