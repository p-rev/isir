#ifndef __ISICG_ISIR_INDEX_TRIANGLE__
#define __ISICG_ISIR_INDEX_TRIANGLE__

#include "../defines.hpp"

namespace ISICG_ISIR {
    struct IndexTriangle
	{
		// vertices ids in TriangleMesh::_vertices
		union
		{
			struct
			{
				uint _v0, _v1, _v2;
			};
			uint _v[3];
		};

        union
        {
            struct
            {
                uint _n0, _n1, _n2;
            };
            uint _n[3];
        };
	};
}

#endif // __ISICG_ISIR_INDEX_TRIANGLE__
