#include "intersection.hpp"

namespace ISICG_ISIR
{
	bool Intersection::operator<(const Intersection& o) {
		return this->_distance < o._distance;
	}
}