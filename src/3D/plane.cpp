#include "plane.hpp"

namespace ISICG_ISIR
{
	std::vector<Intersection> Plane::intersect(const Ray &ray) 
	{
		std::vector<Intersection> inter;

		float denom = glm::dot(_normal, ray._direction);

		if(denom != 0) {
			float t = -(glm::dot(_normal, ray._position) + _d)/denom;
			if(t >= 0) {
				inter.push_back(Intersection(_normal, t, this));
			}
		}

		return inter;
	}

} // namespace ISICG_ISIR
