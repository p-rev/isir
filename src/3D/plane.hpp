#ifndef __ISICG_ISIR_PLAN__
#define __ISICG_ISIR_PLAN__

#include <vector>

#include "aObject3D.hpp"

namespace ISICG_ISIR
{
	class Plane : public AObject3D
	{
	public:
		Plane() = default;
		Plane(const Vec3f &p0, const Vec3f &normal) : _p0(p0) {
			_normal = glm::normalize(normal);
			_d = -glm::dot(_normal, _p0);
		}

		std::vector<Intersection> intersect(const Ray &ray) override;

	private:
		Vec3f _p0 = VEC3F_ZERO;
		Vec3f _normal = Vec3f(0.0f, 1.0f, 0.0f);
		float _d = 0.0f;
	};
} // namespace ISICG_ISIR

#endif //__ISICG_ISIR_PLAN__
