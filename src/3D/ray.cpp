#include "ray.hpp"

#include <algorithm>

#include "aObject3D.hpp"
#include "intersection.hpp"
#include "../lights/aLight.hpp"
#include "../materials/aMaterial.hpp"

namespace ISICG_ISIR
{
    Vec3f Ray::trace(const std::vector<AObject3D *> &objects,
                     const std::vector<ALight *> &lights,
                     uint iteration)
    {
        if(iteration <= 0) {
//			return Vec3f(1.0f, 0.0f, 1.0f); // magenta
            return Vec3f(0.0f);
		}

        Ray& r = *this;

		std::vector<Intersection> inter;
		for (uint o = 0; o < objects.size(); ++o) {
			if (objects[0]) {
				std::vector<Intersection> o_inter = objects[o]->intersect(r);
				inter.insert(inter.end(), o_inter.begin(), o_inter.end());
			}
		}

		if (inter.size() > 0) {
			auto closest_inter = std::min_element(inter.begin(), inter.end());
			return closest_inter->_obj->getMaterial()->shade(r, *closest_inter, objects, lights, --iteration);
		} else {
            return Vec3f(0.f);
		}
    }
}
