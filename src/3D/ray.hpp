#ifndef __ISICG_ISIR_RAY__
#define __ISICG_ISIR_RAY__

#include <vector>

#include "../defines.hpp"

namespace ISICG_ISIR
{
	class AObject3D;
	class ALight;

	class Ray
	{
	public:
		Ray(const Vec3f &position, const Vec3f &direction)
			: _position(position), _direction(direction)
		{
		}

		Vec3f trace(const std::vector<AObject3D *> &objects,
			        const std::vector<ALight *> &ligths,
					uint iteration);

	//private:
		Vec3f _position;
		Vec3f _direction;
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_RAY__
