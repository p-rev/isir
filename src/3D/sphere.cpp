#include "sphere.hpp"

namespace ISICG_ISIR
{
	// see
	// https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection?url=3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-sphere-intersection
	std::vector<Intersection> Sphere::intersect(const Ray &ray)
	{
		std::vector<Intersection> inter;

		Vec3f CO = ray._position - _center;
		float CO_length = glm::length(CO);

		//float a = 1.0f;
		// a is always equal to 1
		float b = 2.0f * glm::dot(ray._direction, CO);
		float c = CO_length * CO_length - _radius * _radius;

		float delta = b * b - 4.0f * c;
		if (delta > 0)
		{
			float sqrt_delta = sqrtf(delta);
			float t0 = (-b + sqrt_delta) / 2.0f;
			float t1 = (-b - sqrt_delta) / 2.0f;

			if (t0 >= 0) addIntersection(inter, ray, t0);
			if (t1 >= 0) addIntersection(inter, ray, t1);
		}
		else if (delta == 0)
		{
			float t = -b / 2.0f;

			if (t >= 0) addIntersection(inter, ray, t);
		}

		return inter;
	}

	void Sphere::addIntersection(std::vector<Intersection> &inter,
								 const Ray &ray, float distance)
	{
		Vec3f p = ray._position + ray._direction * distance;

		//Vec3f n = glm::normalize(p - _center);
		// the length of the vector (_center, p) equals to the radius of the sphere
		// to normalize this vector, we jus tneed to divide it by the radius
		Vec3f n = (p - _center) / _radius;

		inter.push_back(Intersection(n, distance, this));
	}
} // namespace ISICG_ISIR
