#ifndef __ISICG_ISIR_SPHERE__
#define __ISICG_ISIR_SPHERE__

#include <vector>

#include "aObject3D.hpp"

namespace ISICG_ISIR
{
	class Sphere : public AObject3D
	{
	public:
		Sphere() = default;
		Sphere(const Vec3f &center, const float radius)
			: _center(center), _radius(radius)
		{
		}

		std::vector<Intersection> intersect(const Ray &ray) override;
		/*{
			return std::vector<Intersection>();
		}*/

	private:
		Vec3f _center = VEC3F_ZERO;
		float _radius = 1.f;

		void addIntersection(std::vector<Intersection> &inter, const Ray &ray, float distance);
	};
} // namespace ISICG_ISIR

#endif //__ISICG_ISIR_SPHERE__
