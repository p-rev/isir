#include "triangleMesh.hpp"

#include <iostream>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>


namespace ISICG_ISIR
{
	// Load a mesh with assimp
	void TriangleMesh::load(const std::string &path)
	{
		Assimp::Importer importer;

		// Read scene and triangulate meshes
		const aiScene *const scene
            = importer.ReadFile(path.c_str(), aiProcess_Triangulate);

		if (scene == nullptr)
		{
			throw std::runtime_error("Cannot import file: " + path);
		}

		const uint nbMeshes = scene->mNumMeshes;
		uint nbTriangles = 0;
        uint nbVertices = 0;

		for (uint i = 0; i < nbMeshes; ++i)
		{
			const aiMesh *const mesh = scene->mMeshes[i];
			nbTriangles += mesh->mNumFaces;
            nbVertices += mesh->mNumVertices;
		}

		_triangles.resize(nbTriangles);
		_vertices.resize(nbVertices);
        _normals.resize(nbVertices);

		uint currentTriangle = 0;
		uint currentVertex = 0;
        uint currentNormal = 0;

		for (uint m = 0; m < nbMeshes; ++m)
		{
			const aiMesh *const mesh = scene->mMeshes[m];
			const aiMaterial *const material
				= scene->mMaterials[mesh->mMaterialIndex];

			for (uint f = 0; f < mesh->mNumFaces; ++f, ++currentTriangle)
			{
				const aiFace face = mesh->mFaces[f];

				IndexTriangle &tri = _triangles[currentTriangle];

				// triangulated ! :-)
				for (uint v = 0; v < 3; ++v)
				{
					const uint idV = face.mIndices[v];
					tri._v[v] = idV;
                    tri._n[v] = idV;
				}
			}

			for (uint v = 0; v < mesh->mNumVertices; ++v, ++currentVertex)
			{
				Vec3f &vertex = _vertices[currentVertex];
				vertex.x = mesh->mVertices[v].x;
				vertex.y = mesh->mVertices[v].y;
				vertex.z = mesh->mVertices[v].z;
				//_box.enlarge(vertex);
			}

            for (uint n = 0; n < mesh->mNumVertices; ++n, ++currentNormal)
            {
                Vec3f &normal = _normals[currentNormal];
                normal.x = mesh->mNormals[n].x;
                normal.y = mesh->mNormals[n].y;
                normal.z = mesh->mNormals[n].z;
            }
		}

		std::vector<IndexTriangle*>* triangles =
        new std::vector<IndexTriangle*>(_triangles.size());
		for (uint i = 0; i < _triangles.size(); ++i) {
			(*triangles)[i] = &_triangles[i];
		}
		_BVHroot.create(triangles, _vertices);

		std::string boxInfo;
		//_box.toString(boxInfo);
		std::cout << "Loaded: " << std::endl
				  << "- nb faces: " << _triangles.size() << std::endl
				  << "- nb vertices: " << _vertices.size() << std::endl
				  << boxInfo;
	}

	std::vector<Intersection> TriangleMesh::intersect(const Ray &ray)
	{
		std::vector<Intersection> inter;

		std::vector<IndexTriangle*> intersect_triangles;
		_BVHroot.intersect(ray, intersect_triangles);

		// if(_box.intersect(ray)) {
		for (auto i = intersect_triangles.begin(); i != intersect_triangles.end(); i++) {
			IndexTriangle *tri = *i;

            const Vec3f &v0 = _vertices[tri->_v0];
            const Vec3f &v1 = _vertices[tri->_v1];
            const Vec3f &v2 = _vertices[tri->_v2];

            const Vec3f &n0 = _normals[tri->_n0];
            const Vec3f &n1 = _normals[tri->_n1];
            const Vec3f &n2 = _normals[tri->_n2];

			Vec3f va = v1 - v0;
			Vec3f vb = v2 - v1;
			Vec3f vc = v0 - v2;

            Vec3f plane_normal = glm::cross(v1 - v0, v2 - v0);
            float triangle_area = glm::length(plane_normal) / 2.0f;
            plane_normal = glm::normalize(plane_normal);
            float denom = glm::dot(plane_normal, ray._direction);
            float d = -glm::dot(plane_normal, v0);

			if(denom != 0) {
                float t = -(glm::dot(plane_normal, ray._position) + d)/denom;
				if(t >= 0) {
					Vec3f p = ray._position + ray._direction * t;
					Vec3f pa = p - v0;
					Vec3f pb = p - v1;
					Vec3f pc = p - v2;
					Vec3f ca = glm::cross(va, pa);
					Vec3f cb = glm::cross(vb, pb);
					Vec3f cc = glm::cross(vc, pc);

                    if (glm::dot(plane_normal, ca) >= 0 &&
                        glm::dot(plane_normal, cb) >= 0 &&
                        glm::dot(plane_normal, cc) >= 0) {

                        // Triangle v2 v1 p area
                        float u = glm::length(glm::cross(v1 - v2, p  - v2)) / 2.0f / triangle_area;

                        // Triangle v0 v2 p area
                        float v = glm::length(glm::cross(v2 - v0, p  - v0)) / 2.0f / triangle_area;

                        float w = 1.0f - u - v;
                        Vec3f normal = u * n0 + v * n1 + w * n2;

                        inter.push_back(Intersection(normal, t, this));
                    }
				}
			}
		}
		// }

		return inter;
	}
} // namespace ISICG_ISIR
