#ifndef __ISICG_ISIR_TRIANGLE_MESH__
#define __ISICG_ISIR_TRIANGLE_MESH__

#include <vector>

#include "assimp/mesh.h"

#include "../defines.hpp"
#include "aObject3D.hpp"
#include "boundingBox.hpp"
#include "indexTriangle.hpp"
#include "BVHnode.hpp"

namespace ISICG_ISIR
{
	class TriangleMesh : public AObject3D
	{
	public:
		TriangleMesh() = default;

		void load(const std::string &path);

		std::vector<Intersection> intersect(const Ray &ray) override;

	private:
		BoundingBox _box;
		BVHnode _BVHroot;
		std::vector<IndexTriangle> _triangles;
		std::vector<Vec3f> _vertices;
        std::vector<Vec3f> _normals;
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_TRIANGLE_MESH__
