#include "camera.hpp"

namespace ISICG_ISIR
{
    Camera::Camera(const Vec3f &from, const Vec3f &to, int width, int height, float focale) {
		_ratio = (float)height / (float)width;
		_focale = focale;

        _position = from;
        _direction = glm::normalize(to - from);
        _left = glm::cross(Vec3f(.0f, 1.f, .0f), _direction);
        _up = glm::cross(_direction, _left);


		_screen_center = _position + _direction * _focale;
	}

	Ray Camera::generateRay(Vec3f pixel_pos) {
		float x = pixel_pos.x - 0.5f;
		float y = pixel_pos.y - 0.5f;

		Vec3f world_pos = _screen_center + x * _left + y  * _ratio * (-_up);

		Ray r(_position, glm::normalize(world_pos - _position));
		return r;
	}
}
