#ifndef __ISICG_ISIR_CAMERA__
#define __ISICG_ISIR_CAMERA__

#include "3D/ray.hpp"

namespace ISICG_ISIR
{
	class Camera
	{
	public:
        Camera(const Vec3f &from, const Vec3f &to, int width, int height, float focale);

		Ray generateRay(Vec3f pixel_pos);
		//static Vec3f computeColor(const Ray &r, const std::vector<AObject3D *> &objects, uint iteration);

	private:
		Vec3f _position = VEC3F_ZERO;
		Vec3f _direction = Vec3f(0.0f, 0.0f, 1.0f);
		Vec3f _up = Vec3f(0.0f, 1.0f, 0.0f);
		Vec3f _left = VEC3F_ZERO;
		Vec3f _screen_center = VEC3F_ZERO;
		float _focale;
		float _ratio;
	};
} // namespace ISICG_ISIR

#endif //__ISICG_ISIR_CAMERA__
