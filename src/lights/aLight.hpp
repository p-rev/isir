#ifndef __ISICG_ISIR_A_LIGHT__
#define __ISICG_ISIR_A_LIGHT__

#include "../defines.hpp"
#include "../3D/aObject3D.hpp"

namespace ISICG_ISIR {
    class ALight {

    public:
        virtual Vec3f getPos() const {
            return _pos;
        }

        virtual Vec3f getColor() const {
            return _color;
        }

        virtual Vec3f getIntensity(const Vec3f& p, const Vec3f& n) const = 0;
        virtual bool inShadow(const Vec3f& p, const std::vector<AObject3D *> &objects) const = 0;

    protected:
        Vec3f _pos;
        Vec3f _color;
    };
}

#endif // __ISICG_ISIR_A_LIGHT__
