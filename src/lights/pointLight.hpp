#ifndef __ISICG_ISIR_POINT_LIGHT__
#define __ISICG_ISIR_POINT_LIGHT__

#include <algorithm>

#include "aLight.hpp"
#include "../3D/ray.hpp"
#include "../3D/intersection.hpp"
#include "../3D/aObject3D.hpp"

namespace ISICG_ISIR {
    class PointLight : public ALight {
        public:

        PointLight(Vec3f pos, Vec3f color, float intensity) {
            _pos = pos;
            _color = color;
            _intensity = intensity;
        }

        Vec3f getIntensity(const Vec3f& p, const Vec3f& n) const
        {
            Vec3f l_dir = _pos - p;
            float l_dist = glm::length(l_dir);
            l_dir /= l_dist; // normalization

            float attenuation = 1.0f / (l_dist * l_dist);
            if (attenuation != attenuation) // if (attenuation == NaN)
                attenuation = 0.0f;

            return glm::max(_color * glm::dot(l_dir, n), Vec3f(0.0f)) * _intensity * attenuation;
        }

        bool inShadow(const Vec3f& p, const std::vector<AObject3D *> &objects) const {
            Vec3f l_dir = _pos - p;
            float l_dist = glm::length(l_dir);
            l_dir /= l_dist; // normalization

            Ray shadow_ray(p + EPSILON * l_dir, l_dir);
            for (auto i = objects.begin(); i != objects.end(); ++i) {
                std::vector<Intersection> inter = (*i)->intersect(shadow_ray);

                if (inter.size() > 0) {
                    auto closest_inter = std::min_element(inter.begin(), inter.end());
                    if (closest_inter->_distance < l_dist) {
                        return true;
                    }
                }
            }

            return false;
        }

        protected:
            float _intensity;
    };
}

#endif // __ISICG_ISIR_POINT_LIGHT__
