#include <algorithm>
#include <iostream>
#include <random>
#include <omp.h>

#include "camera.hpp"
#include "chrono.hpp"
#include "defines.hpp"
#include "image.hpp"
#include "3D/sphere.hpp"
#include "3D/plane.hpp"
#include "3D/triangleMesh.hpp"
#include "3D/implicit.hpp"
#include "lights/aLight.hpp"
#include "lights/pointLight.hpp"
//#include "materials/flatColorMaterial.hpp"
#include "materials/lambertMaterial.hpp"
#include "materials/brdfMaterial.hpp"
#include "materials/brdf.hpp"
#include "materials/normalMaterial.hpp"
#include "materials/pbrMaterial.hpp"

namespace ISICG_ISIR
{
	int main(int argc, char **argv)
	{
        if (argc != 6)
		{
			throw std::runtime_error(
				"Invalid program argument: must be launched with"
                "<imageName> <imageHeight> <imageWidth> <maxIteration> <superSampling>\n"
                "e.g.results.jpg 800 600 5 2");
		}

		// get arguments
		const std::string name(argv[1]);
		const uint width = atoi(argv[2]);
		const uint height = atoi(argv[3]);
		const uint max_iteration = atoi(argv[4]);
        const uint super_sampling = atoi(argv[5]);

        const float super_sampling_inc = 1.0f / (float) super_sampling;
        const float super_sampling_sq = super_sampling * super_sampling;

		ImageJPG image(width, height);

        Camera camera(Vec3f(0.f, 2.f, -15.f), Vec3f(0.f, 2.f, 15.0f), width, height, 2.0f);

        LambertMaterial mtlTiles(0.0f, Vec3f(0.0f, 0.0f, 0.0f), 1.0f, Vec3f(1.0f, .4f, 0.0f), .8f, 0.0f, true);
        LambertMaterial mtlMirror(1.0f, Vec3f(1.0f), 0.f, Vec3f(0.8, 0.8, 1.f), 1.0f, .0f ,false);
        PbrMaterial mtlWhiteDiffuse(Vec3f(1.0f), 0.0f, 0.9);

        NormalMaterial mtlNormal;
        PbrMaterial mtlPbr(Vec3f(1.0f, 0.71f, 0.29f), 1.f, 0.4f);

        Plane plane(Vec3f(0.01f, .01f, 0.01f), Vec3f(0.0f, 1.0f, 0.0f));
        plane.setMaterial(&mtlTiles);
        Plane backPlane(Vec3f(0.f, 0.f, 25.f), Vec3f(0.0f, 0.0f, -1.0f));
        backPlane.setMaterial(&mtlWhiteDiffuse);
        Plane frontPlane(Vec3f(0.f, 0.f, -25.f), Vec3f(0.0f, 0.0f, 1.0f));
        frontPlane.setMaterial(&mtlWhiteDiffuse);
        Plane rightPlane(Vec3f(5.f, 0.f, 0.f), glm::normalize(Vec3f(-1.0f, 0.0f, 0.0f)));
        rightPlane.setMaterial(&mtlMirror);

        TriangleMesh suzanne;
        suzanne.load("data/obj/suzanne_final.obj");
        suzanne.setMaterial(&mtlPbr);

        /* Pig */
        TriangleMesh pig;
        pig.load("data/obj/pig_final.obj");

        PbrMaterial mtlPig(Vec3f(1.f, .5f, .5f), 0.f, 0.9f);
        pig.setMaterial(&mtlPig);

        /* Glass */
        TriangleMesh glass;
        glass.load("data/obj/glass_final.obj");

        LambertMaterial mtlGlass(1.0f, Vec3f(0.f), 0.2f, Vec3f(.8f, .9f, 1.f), .05f, 1.f, false);
        glass.setMaterial(&mtlGlass);

        Implicit implicit(Vec3f(2.f, 1.5f, 15.f), 3.0f);
        PbrMaterial mtlBlob(Vec3f(1.0f), 0.f, 1.f);
        implicit.setMaterial(&mtlMirror);


		std::vector<AObject3D *> objects;
        objects.emplace_back(&suzanne);
        objects.emplace_back(&pig);
        objects.emplace_back(&glass);
        objects.emplace_back(&plane);
        objects.emplace_back(&backPlane);
        objects.emplace_back(&frontPlane);
        objects.emplace_back(&rightPlane);
        objects.emplace_back(&implicit);

        PointLight lightWhite(Vec3f(1.0f, 3.0f, 10.0f), Vec3f(1.0f), 600.f);
        PointLight lightRed(Vec3f(3.0f, 3.0f, 10.0f), Vec3f(1.0f, 0.f, 0.f), 100.f);
        PointLight lightBlue(Vec3f(-3.0f, 3.0f, 10.0f), Vec3f(0.f, 0.f, 1.0f), 100.f);

		std::vector<ALight *> lights;
        lights.emplace_back(&lightWhite);
        lights.emplace_back(&lightRed);
        lights.emplace_back(&lightBlue);

		// random
		std::random_device rd;
		std::mt19937 gen(rd()); // mersenne_twister_engine seeded with rd()
		std::uniform_real_distribution<float> dis(0.f, 1.f);

		Chrono chrono;

		std::cout << "Rendering..." << std::endl;
		std::cout << "v        v" << std::endl;
		const uint nb_pixels = width * height;
		const uint progress_unit = nb_pixels / 10;
		chrono.start();
		// rendering loop

        #pragma omp parallel for
		for (uint h = 0; h < height; ++h)
		{
			for (uint w = 0; w < width; ++w)
			{
                Vec3f color(0.0f);

                for (uint y = 0; y < super_sampling; ++y) {
                    for (uint x = 0; x < super_sampling; ++x) {
                        Vec3f pixel_pos(
                                    ((float)w + x * super_sampling_inc) / (float)width,
                                    ((float)h + y * super_sampling_inc) / (float)height,
                                    0.0f);

                        Ray r = camera.generateRay(pixel_pos);
                        color += r.trace(objects, lights, max_iteration);
                    }
                }

                color /= super_sampling_sq;
                color /= (color + Vec3f(1.0f)); // reinhard
//                color = glm::pow(color, Vec3f(1.0f / 2.2f)); // gamma correction

				image.setPixel(w, h, Vec4f(color, 1.0f));

				uint i = h * width + w;
				if(i > 0 && i % progress_unit == 0) {
					std::cout << "#" << std::flush;
				}
			}
		}

		std::cout << "#" << std::endl;

		chrono.stop();
		std::cout << "Rendering done. Image computed in "
				  << chrono.elapsedTime() << "s (" << image.getWidth() << "x"
				  << image.getHeight() << ")" << std::endl;

		std::cout << "Save image as: " << name << std::endl;
		image.save(name);

		return EXIT_SUCCESS;
	}
} // namespace ISICG_ISIR

int main(int argc, char **argv)
{
	try
	{
		return ISICG_ISIR::main(argc, argv);
	}
	catch (const std::exception &e)
	{
		std::cerr << "Exception caught !" << std::endl << e.what() << std::endl;
		return EXIT_FAILURE;
	}
}
