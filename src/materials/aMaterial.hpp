#ifndef __ISICG_ISIR_A_MATERIAL__
#define __ISICG_ISIR_A_MATERIAL__

#include <vector>

#include "../defines.hpp"

#include "../3D/intersection.hpp"
#include "../3D/ray.hpp"
#include "../3D/aObject3D.hpp"
#include "../lights/aLight.hpp"

namespace ISICG_ISIR
{
	class AMaterial
	{
	public:
        virtual Vec3f shade(const Ray &ray,
                      const Intersection &intersection,
                      const std::vector<AObject3D *> &objects,
                      const std::vector<ALight *> &lights,
                      uint iteration) = 0;
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_A_MATERIAL__
