#include "brdf.hpp"

#include <iostream>

namespace ISICG_ISIR {
    Brdf::Brdf(float roughness, const Vec3f &f0) : _roughness(roughness), _f0(f0)
    {}

    Vec3f Brdf::f(const Vec3f &wi, const Vec3f &wo, const Vec3f &n, Vec3f *f) const {
        Vec3f h = glm::normalize(wi + wo);
        float nwi = glm::dot(n, wi);
        float nwo = glm::dot(n, wo);

        Vec3f F = schlickFresnel(wi, h);
        float G = geometryFunction(n, h, wo, wi);
        float D = beckmannDistribution(h, n);
        Vec3f result = F * G * D / (4.0f * nwi * nwo);

        if (f) *f = F;

        return result;
    }

    Vec3f Brdf::schlickFresnel(const Vec3f &wi, const Vec3f &h) const {
        return _f0 + (1.0f - _f0) * glm::pow(1.0f - glm::dot(wi, h), 5.0f);
    }

    float Brdf::geometryFunction(const Vec3f &wi, const Vec3f &wo) const {
        float lwi = lambda(wi);
        float lwo = lambda(wo);

        return 1.0f / (1.0f + lwi + lwo);
    }

    float Brdf::geometryFunction(const Vec3f  n, const Vec3f h, const Vec3f v, const Vec3f l) const {
        float NdotV= dot(n, v);
        float NdotL= dot(n, l);
        float HdotV= dot(h, v);
        float HdotL= dot(h, l);
        float NdotV_clamped= glm::max(NdotV, 0.0f);
        float a= 1.0f/ ( _roughness * tan( acos(NdotV_clamped) ) );
        float a_Sq= a* a;
        float a_term;
        if (a<1.6f)
            a_term= (3.535f * a + 2.181f * a_Sq)/(1.0f + 2.276f * a + 2.577f * a_Sq);
        else
            a_term= 1.0f;
        return  ( glm::step(0.0f, HdotL/NdotL) * a_term  ) *
                ( glm::step(0.0f, HdotV/NdotV) * a_term  ) ;
    }

    float Brdf::beckmannDistribution(const Vec3f &h, const Vec3f &n) const {
        float alpha = _roughness * _roughness;
        float alpha2 = alpha * alpha;
        float nh = glm::dot(n, h);
        float nh2 = nh * nh;

        return glm::exp((nh2 - 1.0f) / (alpha2 * nh2)) / (PI * alpha2 * nh2 * nh2);
    }

    float Brdf::lambda(const Vec3f& v) const {
        float absTanTheta = glm::abs(glm::sqrt(tan2Theta(v)));
        float m_alpha = _roughness; // ?
        float a = 1.0f / (m_alpha * absTanTheta);

        if ( a >= 1.6f) return 0.0f;

        return (1.0f - 1.259f * a + 0.396f * a * a) / (3.535f * a + 2.181f * a * a);
    }
}
