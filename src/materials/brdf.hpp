#ifndef __ISICG_ISIR_BRDF__
#define __ISICG_ISIR_BRDF__

#include "../defines.hpp"

namespace ISICG_ISIR {
    class Brdf
    {
    public:
        Brdf(float _roughness, const Vec3f &f0);
        Vec3f f(const Vec3f &wi, const Vec3f &wo, const Vec3f &n, Vec3f *f = nullptr) const;

    protected:
        float geometryFunction(const Vec3f &wi, const Vec3f &wo) const;
        float geometryFunction(const Vec3f  n, const Vec3f h, const Vec3f v, const Vec3f l) const;
        float lambda(const Vec3f& v) const;
        float beckmannDistribution(const Vec3f &h, const Vec3f &n) const;
        Vec3f schlickFresnel(const Vec3f &wi, const Vec3f &h) const;

        static float cos2Theta(const Vec3f& v) { return v.z * v.z; }
        static float sin2Theta(const Vec3f& v) { return glm::max(0.0f, 1 - cos2Theta(v)); }
        static float tan2Theta(const Vec3f& v) { return sin2Theta(v) / cos2Theta(v); };

        float _roughness;
        Vec3f _f0;
    };
}
#endif // __ISICG_ISIR_BRDF__
