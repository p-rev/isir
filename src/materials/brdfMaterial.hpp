#ifndef __ISICG_ISIR_BRDF_MATERIAL__
#define __ISICG_ISIR_BRDF_MATERIAL__

#include "aMaterial.hpp"
#include "brdf.hpp"

namespace ISICG_ISIR {
    class BrdfMaterial : public AMaterial
    {
    public:
        BrdfMaterial(Brdf* brdf) : _brdf(brdf) {}

        Vec3f shade(const Ray &ray,
                    const Intersection &intersection,
                    const std::vector<AObject3D *> &objects,
                    const std::vector<ALight *> &lights,
                    uint iteration)
        {
            Vec3f color(0.0f);

            for (auto l = lights.begin(); l != lights.end(); ++l) {
                Vec3f p = ray._position + ray._direction * intersection._distance;
                Vec3f wo = glm::normalize(ray._position - p);
                Vec3f wi = glm::normalize((*l)->getPos() - p);

                if (!(*l)->inShadow(p, objects))
                    color += glm::max(Vec3f(0.0f), _brdf->f(wi, wo, intersection._normale) * (*l)->getIntensity(p, intersection._normale));
            }

            return color;
        }

    protected:
        Brdf* _brdf;
    };
}

#endif // __ISICG_ISIR_BRDF_MATERIAL__
