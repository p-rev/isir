#ifndef __ISICG_ISIR_FLAT_COLOR_MATERIAL__
#define __ISICG_ISIR_FLAT_COLOR_MATERIAL__

#include "aMaterial.hpp"

namespace ISICG_ISIR
{
	class FlatColorMaterial : public AMaterial
	{
	public:
		FlatColorMaterial(const Vec3f &color) : _color(color) {}
        virtual Vec3f shade(const Ray &ray,
                      const Intersection &intersection,
                      const std::vector<AObject3D *> &objects,
                      const std::vector<ALight *> &lights,
                      uint iteration)
		{
			return _color;
		}
	private:
		Vec3f _color = VEC3F_ZERO;
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_FLAT_COLOR_MATERIAL__
