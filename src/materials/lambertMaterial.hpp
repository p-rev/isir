#ifndef __ISICG_ISIR_LAMBERT_MATERIAL__
#define __ISICG_ISIR_LAMBERT_MATERIAL__

#include "aMaterial.hpp"

namespace ISICG_ISIR
{
	class LambertMaterial : public AMaterial
	{
	public:
		LambertMaterial(float ka, const Vec3f &ambient, float kd, const Vec3f &diffuse, float reflection, float refraction, bool checker)
			: _ka(ka), _kd(kd), _ca(ambient), _cd(diffuse), _krefl(reflection), _krefr(refraction), _checker(checker)
		{
		}

        virtual Vec3f shade(const Ray &ray,
					const Intersection &intersection,
					const std::vector<AObject3D *> &objects,
					const std::vector<ALight *> &lights,
					uint iteration)
		{
			Vec3f inter_pos = ray._position + ray._direction * intersection._distance;
			Vec3f reflection;
			Vec3f refraction;
			Vec3f phong;
            Vec3f color(0);

			/* reflections */
			if (_krefl) {
				Vec3f reflected = glm::reflect(ray._direction, intersection._normale);
				Ray reflected_ray(inter_pos + EPSILON * reflected, reflected);
				reflection = reflected_ray.trace(objects, lights, iteration);
			}

			/* refraction */
			if (_krefr) {
				float eta = 1.5f;
				Vec3f refract_normal = intersection._normale;
				if (glm::dot(ray._direction, intersection._normale) > 0.0f) {
					refract_normal = -refract_normal;
				} else {
					eta = 1.0f / eta;
				}
				Vec3f refracted = glm::refract(ray._direction, refract_normal, eta);
				if (refracted == Vec3f(0.0f)) {
					refracted = glm::reflect(ray._direction, refract_normal);
				}

				Ray refracted_ray(inter_pos + EPSILON * refracted, refracted);
				refraction = refracted_ray.trace(objects, lights, iteration);
			}


			Vec3f cd = _cd;
			if (_checker) {
				bool x = (int)floor(inter_pos.x * 2.0f) % 2 == 0;
				bool y = (int)floor(inter_pos.y * 2.0f) % 2 == 0;
				bool z = (int)floor(inter_pos.z * 2.0f) % 2 == 0;

				if (x xor y xor z) {
                    cd = Vec3f(0.f);
				}
			}

			for(auto l_it = lights.begin(); l_it != lights.end(); ++l_it) {
				Vec3f l_intensity = (*l_it)->getIntensity(inter_pos, intersection._normale);

                bool inShadow = (*l_it)->inShadow(inter_pos, objects);

				if (inShadow) {
					phong = _ka * _ca;
				} else {
                    phong = _ka * _ca + _kd * l_intensity *  cd;
				}
                color += (1.0f - _krefl - _krefr) * phong;
			}

            color += _krefl * reflection * _cd + _krefr * refraction;
			return glm::max(color, Vec3f(0.0f));
		}

	private:
		float _ka = 0.0f;
		float _kd = 1.0f;
		Vec3f _ca = VEC3F_ZERO;
		Vec3f _cd = VEC3F_ZERO;

		float _krefl = 0.0f;
		float _krefr = 0.0f;
		bool _checker = false;
	};
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_LAMBERT_MATERIAL__
