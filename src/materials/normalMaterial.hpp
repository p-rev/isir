#ifndef __ISICG_ISIR_NORMAL_MATERIAL__
#define __ISICG_ISIR_NORMAL_MATERIAL__

#include "aMaterial.hpp"

namespace ISICG_ISIR
{
    class NormalMaterial : public AMaterial
    {
    public:
        NormalMaterial() {}
        virtual Vec3f shade(const Ray &ray,
                      const Intersection &intersection,
                      const std::vector<AObject3D *> &objects,
                      const std::vector<ALight *> &lights,
                      uint iteration)
        {
            return glm::abs(intersection._normale);
        }
    };
} // namespace ISICG_ISIR

#endif // __ISICG_ISIR_NORMAL_MATERIAL__
