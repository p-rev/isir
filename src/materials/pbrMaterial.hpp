#ifndef __ISICG_ISIR_PBR_MATERIAL__
#define __ISICG_ISIR_PBR_MATERIAL__

#include "aMaterial.hpp"
#include "brdf.hpp"

namespace ISICG_ISIR {
    class PbrMaterial : public AMaterial
    {
    public:
        //PbrMaterial(Brdf* brdf) : _brdf(brdf) {}
        PbrMaterial(const Vec3f &albedo, float metallic, float roughness)
            : _albedo(albedo), _metallic(metallic), _roughness(roughness)
        {
            Vec3f f0(0.04);
            f0 = glm::mix(f0, _albedo, _metallic);
            _brdf = new Brdf(_roughness, f0);
        }

        Vec3f shade(const Ray &ray,
                    const Intersection &intersection,
                    const std::vector<AObject3D *> &objects,
                    const std::vector<ALight *> &lights,
                    uint iteration)
        {
            Vec3f color(0.0f);

            for (auto l = lights.begin(); l != lights.end(); ++l) {
                Vec3f p = ray._position + ray._direction * intersection._distance;
                Vec3f wo = glm::normalize(ray._position - p);
                Vec3f wi = glm::normalize((*l)->getPos() - p);

                if (!(*l)->inShadow(p, objects)) {
                    Vec3f f;
                    Vec3f specular = glm::max(Vec3f(0.0f), _brdf->f(wi, wo, intersection._normale, &f));// * (*l)->getIntensity(p, intersection._normale));

                    Vec3f Ks = f;
                    Vec3f Kd = Vec3f(1.f) - Ks;
                    Kd *= 1.f - _metallic;

                    color += (Kd * _albedo / PI + specular) * (*l)->getIntensity(p, intersection._normale);
                            //Lo += (kD * albedo / PI + specular) * radiance * NdotL;
                }
            }

            return color;
        }

    protected:
        Brdf* _brdf;
        Vec3f _albedo;
        float _metallic;
        float _roughness;
    };
}

#endif // __ISICG_ISIR_PBR_MATERIAL__
